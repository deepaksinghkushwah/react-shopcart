import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

import { CategoriesProvider } from "./context/categories/CategoriesContext";
import { UsersProvider } from "./context/users/UsersContext";
import { ProductsProvider } from "./context/products/ProductsContext";

import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Main from "./Layout/Main";
import Home from "./pages/Home";
import NotFound from "./pages/NotFound";
import Categories from "./pages/Categories";
import MemberArea from "./pages/MemberArea";
import Products from "./pages/Products";
import {ProviderComposer,provider} from "./context/ProviderComposer";
import { CartProvider } from "./context/cart/CartContext";
import Cart from "./pages/Cart";
function App() {
  return (
    <div className="App">
      <ProviderComposer
        providers={[
          provider(UsersProvider),
          provider(ProductsProvider),
          provider(CategoriesProvider),
          provider(CartProvider),
        ]}
      >
        <Router>
          <Main>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/categories/list" element={<Categories />} />

              <Route path="/products/:cat_id" element={<Products />} />

              <Route path="/members-area" element={<MemberArea />} />
              <Route path="/cart" element={<Cart />} />
              <Route path="*" element={<NotFound />} />
            </Routes>
          </Main>
        </Router>
      </ProviderComposer>
    </div>
  );
}

export default App;
