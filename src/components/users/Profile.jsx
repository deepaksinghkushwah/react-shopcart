import React, { useState,useContext,useEffect } from 'react';
import UsersContext from '../../context/users/UsersContext';
import { getProfile, saveProfile } from '../../context/users/UsersAction';
import { toast } from 'react-toastify';

function Profile() {
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const {user, dispatch} = useContext(UsersContext);
        
    useEffect(() => {
      const p = async() => {
        dispatch({type: 'SET_LOADING'});
        const g = await getProfile(user.user.id, user.token);
        dispatch({type: 'SET_PROFILE', payload: g});
        setFirstName(g.first_name);
        setLastName(g.last_name);
      }
      p();
    }, [dispatch, user]);
    
    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(first_name, last_name, user.user.id, user.token);
        dispatch({type: 'SET_LOADING'});
        const call = async() => {
            await saveProfile(user.user.id, first_name, last_name, user.token);
            toast.success("Profile saved");
        }

        call();
    }

    const handleChange = (e) => {
        if(e.target.id === "first_name"){
            setFirstName(e.target.value);
        }

        if(e.target.id === "last_name"){
            setLastName(e.target.value);
        }
    }
    return <form onSubmit={handleSubmit}>
        <table className="table table-bordered">
            <tbody>
                <tr>
                    <td><input className='form-control' type="text" id="first_name" value={first_name} placeholder="First Name" onChange={handleChange} /></td>
                </tr>
                <tr>
                    <td><input className='form-control' type="text" id="last_name" value={last_name} placeholder="Last Name" onChange={handleChange} /></td>
                </tr>
                <tr>
                    <td>
                        <button type="submit" className='btn btn-primary'>Save Profile</button>
                    </td>
                </tr>

            </tbody>
        </table>
    </form>;
}

export default Profile;
