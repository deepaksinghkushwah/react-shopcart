import React, { useState, useContext } from 'react';
import { addCategory, getAllCategories } from '../../context/categories/CategoriesAction';
import CategoriesContext from '../../context/categories/CategoriesContext';
import Spinner from "../shared/Spinner";
function AddCategoryForm() {
  const [title, setTitle] = useState('');
  const [image, setImage] = useState();



  const { loading, dispatch } = useContext(CategoriesContext);



  const changeHandler = (e) => {
    setTitle(e.target.value);
  }

  const fileChangeHandler = (e) => {
    setImage(e.target.files[0]);
  }

  const submitHandler = async (e) => {
    e.preventDefault();
    dispatch({ type: 'SET_LOADING' });
    await addCategory(title, image);
    let c = await getAllCategories();
    dispatch({ type: 'GET_CATEGORIES', payload: c });
    setTitle("");
    setImage("");
  }

  return <div className='addCategoryForm'>
    <form onSubmit={submitHandler}>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Add Category</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td><input className='form-control' type='text' id="title" value={title} onChange={changeHandler} placeholder="Enter new category name" /></td>
          </tr>
          <tr>
            <td><input className='form-control' type='file' id="image" onChange={fileChangeHandler} placeholder="Image URL" /></td>
          </tr>
          <tr>
            <td className='text-center'><button className='btn btn-primary' type="submit">Save Category</button></td>
          </tr>
        </tbody>
      </table>
    </form>
  </div>;
}

export default AddCategoryForm;
