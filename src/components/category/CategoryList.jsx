import React from 'react';
import CategoriesContext from "../../context/categories/CategoriesContext";
import { getAllCategories } from '../../context/categories/CategoriesAction';
import { useContext, useEffect } from 'react';
import Spinner from '../shared/Spinner';
import { Link } from 'react-router-dom';
function CategoryList() {
    const { categories, dispatch, loading } = useContext(CategoriesContext);

    useEffect(() => {
        dispatch({ type: 'SET_LOADING' });
        const call = async () => {
            let c = await getAllCategories();
            dispatch({ type: 'GET_CATEGORIES', payload: c });
        }
        call();
    }, [dispatch]);
    if (loading) {
        return <Spinner />
    }
    return (<div className='row row-cols-3'>
        {categories.map(item => (
            <div className='col' key={item.id}>
                <div className="card mb-2">
                    <Link to={`/products/${item.id}`}>
                        <img src={process.env.REACT_APP_CATEGORY_IMAGE_URL+""+item.image} className='img-fluid card-img-top' style={{height: '200px'}} alt={item.title}/>
                    </Link>
                    <div className="card-body">
                        <div className="card-title text-center">{item.title}</div>
                    </div>
                </div>

            </div>
        ))}
    </div>);
}

export default CategoryList;
