import React from 'react';
import ProductItem from "./ProductItem";

function ProductList({ cat_id, products }) {
  if (products.length === 0) {
    return <div>No products found</div>
  }

  return (<>    
    {products && products.map(item => (
      <ProductItem key={item.id} product={item} />
    ))}
  </>);
}

export default ProductList;
