import React from 'react';
import { useContext } from 'react';
import { toast } from 'react-toastify';
import { addItemToCart, getCart } from '../../context/cart/CartAction';
import CartContext from '../../context/cart/CartContext';
import UsersContext from '../../context/users/UsersContext';

function ProductItem({ product }) {
  const { dispatch } = useContext(CartContext);
  const { user } = useContext(UsersContext);
  const addToCart = (product) => {
    dispatch({ type: 'SET_LOADING' });
    const call = async () => {
      await addItemToCart(product, 1, user.token);
      const c = await getCart(user.token);
      dispatch({ type: 'SET_CART', payload: c });
      toast.success("Item added to cart");

    }
    if(!user){
      toast.error("You must login before adding items to your cart");
      return;
    }
    call();
  }

  return <div className="col mb-2">
    <div className="card">
      <div className="card-body">
        <div className="card-title">{product.name}</div>
        <p className='card-text'>
          SKU: {product.sku}<br />
          Price: {product.price}
        </p>
      </div>
      <div className="card-footer text-center">
        <div className="row">
          <div className="col-12"><button type='button' onClick={() => addToCart(product)} className='btn btn-info text-white'>Add To Cart</button></div>
        </div>


      </div>
    </div>
  </div>
}

export default ProductItem;
