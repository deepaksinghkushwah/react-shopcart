import React from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { getCart } from '../../context/cart/CartAction';
import CartContext from '../../context/cart/CartContext';
import UsersContext from '../../context/users/UsersContext';

function MiniCart() {
    const { products, dispatch } = useContext(CartContext);
    const { user } = useContext(UsersContext);
    useEffect(() => {
        dispatch({ type: 'SET_LOADING' });
        const fetchCart = async () => {
            const c = await getCart(user.token);
            console.log(c);
            dispatch({ type: 'SET_CART', payload: c });
        }
        if (user !== null) {
            fetchCart();
        }

        dispatch({ type: 'GET_CART' });
    }, [user, dispatch]);

    if(!user){
        return <></>
    }

    return <div className='card mt-2'>
        <div className="card-header">Cart</div>
        <div className="card-body">

            <div className="card-text">


                {(products && products.length) > 0 && (
                    <div>{products.length} items in your cart</div>
                )}
            </div>
        </div>

    </div>;
}

export default MiniCart;
