import AxiosHTTP from "../AxiosHttp";

export const addItemToCart = async(product, qty, token) => {
    const params = new URLSearchParams({product_id: product.id, qty: qty, token: token});
    const response = await AxiosHTTP.post('/carts',params);
    return await response.data;
}

export const getCart = async(token) => {    
    const response = await AxiosHTTP.get('/carts',{
        headers: {'Authorization': `Bearer ${token}`}
    });
    return await response.data;
}