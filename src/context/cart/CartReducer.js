const CartReducer = (state, action) => {
  switch (action.type) {
    case "ADD_ITEM_TO_CART":
      return {
        ...state,
        products: [...state.products, action.payload],
        loading: false,
      };
    case "SET_CART":
      return {
        ...state,
        products: action.payload,
        loading: false,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
};

export default CartReducer;
