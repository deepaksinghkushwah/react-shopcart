import { createContext, useReducer } from "react";
import CategoriesReducer from "./CategoriesReducer";

const CategoriesContext = createContext();

export const CategoriesProvider = ({ children }) => {
  const initialState = {
    categories: [],
    loading: false,
  };

  const [state, dispatch] = useReducer(CategoriesReducer, initialState);

  return (
    <CategoriesContext.Provider 
      value={{
        ...state,
        dispatch,
      }}
    >
      {children}
    </CategoriesContext.Provider>
  );
};

export default CategoriesContext;
