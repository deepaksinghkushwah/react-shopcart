const CategoriesReducer = (state, action) => {
  switch (action.type) {    
    case "GET_CATEGORIES":
      return {        
        categories: action.payload,
        loading: false,
      };
    case "CLEAR_CATEGORIES":
      return {
        ...state,
        categories: [],
        loading: false,
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
};

export default CategoriesReducer;
