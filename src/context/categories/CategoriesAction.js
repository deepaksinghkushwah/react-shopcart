import AxiosHTTP from "../AxiosHttp";

// get all categories
export const getAllCategories = async() => {    
    const response = await AxiosHTTP.get("/categories");
    return response.data;
}

export const addCategory = async(title, image) => {    
    const data = new FormData();
    data.append('title',title);
    data.append('image', image);
    const response = await AxiosHTTP.post("/categories", data,{
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    });    
    return response.data;
}