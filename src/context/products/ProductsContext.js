import { createContext, useReducer } from "react";
import ProductsReducer from "./ProductsReducer";

const ProductsContext = createContext();

export const ProductsProvider = ({ children }) => {
  const initialState = {
    products: [],
    loading: false,
  };

  const [state, dispatch] = useReducer(ProductsReducer, initialState);

  return (
    <ProductsContext.Provider 
      value={{
        ...state,
        dispatch,
      }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

export default ProductsContext;
