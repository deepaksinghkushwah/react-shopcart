import AxiosHTTP from "../AxiosHttp";

export const getProducts = async(cat_id) => {    
    const p = await AxiosHTTP.get(`/products/${cat_id}`);
    return p.data;
}

export const getProduct = async(product_id) => {    
    const p = await AxiosHTTP.get(`/products/${product_id}`);
    return p.data;
}