import AxiosHTTP from "../AxiosHttp";

export const signUp = () => {};

export const signIn = async (email, password) => {
  const params = new URLSearchParams({ email, password });
  let res = await AxiosHTTP.post("/login", params); //signin or login both are valid
  const data = await res.data;
  return data;
};

export const logoutAction = () => {
  localStorage.removeItem("user");
  //window.location.reload();
};

export const saveProfile = async (user_id, first_name, last_name, token) => {
  const params = new URLSearchParams({ user_id, first_name, last_name, token });
  let res = await AxiosHTTP.post("/profile", params);
  const data = await res.data;
  return data;
};

export const getProfile = async (user_id, token) => {    
  const params = new URLSearchParams({user_id});
  let res = await AxiosHTTP.get("/profile", {
    params,
    headers: {'Authorization': `Bearer ${token}`}
  });
  const data = await res.data;
  return data;
};

export const getUser = (id) => {};
