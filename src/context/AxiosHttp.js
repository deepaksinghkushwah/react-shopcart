import axios from "axios";
const SERVER_HOST = process.env.REACT_APP_API_URL;
const AxiosHTTP = axios.create({
    baseURL: SERVER_HOST
});
export default AxiosHTTP;