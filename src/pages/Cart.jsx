import React from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { getCart } from '../context/cart/CartAction';
import CartContext from '../context/cart/CartContext';
import UsersContext from '../context/users/UsersContext';

function Cart() {
    const { products, dispatch } = useContext(CartContext);
    const { user } = useContext(UsersContext);
    useEffect(() => {
        dispatch({ type: 'SET_LOADING' });
        const fetchCart = async () => {
            const c = await getCart(user.token);
            console.log(c);
            dispatch({ type: 'SET_CART', payload: c });
        }
        if (user !== null) {
            fetchCart();
        }

        dispatch({ type: 'GET_CART' });
    }, [user, dispatch]);


    return <div className=''>
            <h1>Your Cart</h1>
                {(products && products.length) > 0 && (
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Amt</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map(item => (
                                <tr key={item.product_id}>
                                    <td>{item.product_name}</td>
                                    <td>{parseInt(item.product_quantity)}</td>
                                    <td>${parseInt(item.product_price)}</td>
                                    <td>${parseInt(item.product_quantity) * parseInt(item.product_price)}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>

                )}
            </div>
      ;
}

export default Cart;
