import React from 'react';
import CategoryList from '../components/category/CategoryList';
import AddCategoryForm from '../components/category/AddCategoryForm';

function Categories() {
    return (
        <div className="row">            
            <div className="col-9"><CategoryList /></div>
            <div className="col-3"><AddCategoryForm /> </div>
        </div>
    );
}
export default Categories;
