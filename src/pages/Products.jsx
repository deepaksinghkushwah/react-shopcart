import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import ProductList from '../components/products/ProductList';
import { getProducts } from '../context/products/ProductsAction';
import ProductsContext from "../context/products/ProductsContext";

function Products() {
    const { cat_id } = useParams();
    
    const { products, dispatch } = useContext(ProductsContext);
    useEffect(() => {    
        dispatch({ type: 'SET_LOADING' });
        const fetchProducts = async () => {
            let p = await getProducts(cat_id);
            dispatch({ type: 'GET_PRODUCTS', payload: p });
        };
        fetchProducts();
    }, [cat_id]);

    return <>
        <div className="h3 mb-3">Total: {products.length}</div>
        <div className='row row-cols-3'>
            <ProductList cat_id={cat_id} products={products} />
        </div>
    </>;
}

export default Products;
