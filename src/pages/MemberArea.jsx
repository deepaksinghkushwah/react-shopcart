import React, {useContext} from 'react';
import {Tabs, Tab} from "react-bootstrap";
import { Navigate } from 'react-router-dom';
import UsersContext from '../context/users/UsersContext';
import Orders from './Orders';
import Profile from '../components/users/Profile';

function MemberArea() {
    const {user} = useContext(UsersContext);
    return <>
    {user ? <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example" className="mb-3">            
            <Tab eventKey="profile" title="Profile">
                <Profile/>
            </Tab>
            <Tab eventKey="orders" title="Orders">
                <Orders/>
            </Tab>
        </Tabs> : <Navigate to="/"/> }
        
    </>

}

export default MemberArea;
